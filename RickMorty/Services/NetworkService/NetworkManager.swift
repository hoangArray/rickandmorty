//
//  NetworkManager.swift
//  RickMorty
//
//  Created by Anton Hoang on 3/11/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation
import UIKit

protocol Networkable {
    typealias CompletionBlock = ((CharacterInfo) -> ())
    func getAllCharacters(completion: @escaping CompletionBlock)
}

class NetworkManager: Networkable {
    
    func getAllCharacters(completion: @escaping CompletionBlock) {
        let charactersURL = "https://rickandmortyapi.com/api/character"
        let urlString = charactersURL
        let url = URL(string: urlString)!
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            do {
                guard let data = data else { return }
                let decoder = JSONDecoder()
                let result = try decoder.decode(CharacterInfo.self, from: data)
                completion(result)
            } catch {
                print(error)
            }
        }
        task.resume()
    }
}

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            guard let data = try? Data(contentsOf: url),
                let image = UIImage(data: data) else { return }
            DispatchQueue.main.async {
                self?.image = image
            }
        }
    }
}
