//
//  DetailCharacterViewController.swift
//  RickMorty
//
//  Created by Anton Hoang on 3/11/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import UIKit

class DetailCharacterViewController: UIViewController {
    
    //MARK: - Properties
    var tableView = UITableView()
    var charDetails: Character?
    var details: [Details] = [Details]()
    
    //MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        details = [.name, .id, .species, .status, .gender]
    }
    
    //MARK: - setup views
    private func setupUI() {
        title = "Details"
        tableView.dataSource = self
        tableView.register(DetailViewCell.self, forCellReuseIdentifier: "DetailCell")
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
}

extension DetailCharacterViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return details.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell", for: indexPath) as! DetailViewCell
        
        let detailEnum = details[indexPath.row]
        
        guard let name = charDetails?.name,
            let gender = charDetails?.gender,
            let species = charDetails?.species,
            let status = charDetails?.status,
            let id = charDetails?.id else { assert(false) }
        
        switch detailEnum {
        case .name:
            cell.configurate(labelTitle: name, key: "Name")
        case .id:
            cell.configurate(labelTitle: "\(id)", key: "ID")
        case .gender:
            cell.configurate(labelTitle: gender, key: "Gender")
        case .species:
            cell.configurate(labelTitle: species, key: "Species")
        case .status:
            cell.configurate(labelTitle: status, key: "Status")
        }
        return cell
    }
}

enum Details {
    case name
    case status
    case id
    case gender
    case species
}
