//
//  DetailViewCell.swift
//  RickMorty
//
//  Created by Anton Hoang on 3/11/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import UIKit

class DetailViewCell: UITableViewCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .value1, reuseIdentifier: "DetailCell")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configurate(labelTitle: String, key: String) {
        self.textLabel?.text = key
        self.detailTextLabel?.text = labelTitle
    }
}
