//
//  ViewController.swift
//  RickMorty
//
//  Created by Anton Hoang on 3/11/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import UIKit

class ListOfCharactersViewController: UIViewController {
    
    //MARK: - Properties
    var tableView = UITableView()
    var networkManager: Networkable!
    var charactersList = [Character]()
    lazy var sortButton: UIBarButtonItem = {
        let sort = UIBarButtonItem(title: "Sort", style: .plain, target: self, action: #selector(sortByName))
        return sort
    }()
    
    //MARK: - Initialize
    init(networkManager: Networkable) {
        self.networkManager = networkManager
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    //MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getAllCharacters()
        setupSortButton()
    }
    
    //MARK: - setup views
    private func setupSortButton() {
        navigationItem.rightBarButtonItems = [sortButton]
    }
    
    private func setupUI() {
        title = "Characters"
        view.backgroundColor = .clear
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    //MARK: - sort list
    @objc private func sortByName() {
        let sortedList = charactersList.sorted(by: {
            guard let first = $0.name,
                let second = $1.name else { return false }
            return first < second
        })
        charactersList = sortedList
        tableView.reloadData()
    }
    
    //MARK: - get list from network
    private func getAllCharacters() {
        networkManager.getAllCharacters {
            _ = $0.results.map { [unowned self] in
                self.charactersList.append($0)
                DispatchQueue.main.async { [unowned self] in
                    self.tableView.reloadData()
                }
            }
        }
    }
}

extension ListOfCharactersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return charactersList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let character = charactersList[indexPath.row]
        cell.textLabel?.text = character.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let detailViewController = DetailCharacterViewController()
        detailViewController.charDetails = charactersList[indexPath.row]
        navigationController?.pushViewController(detailViewController, animated: true)
    }
}



