//
//  Person.swift
//  RickMorty
//
//  Created by Anton Hoang on 3/11/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation

struct CharacterInfo: Codable {
    var results: [Character]
}

struct Character: Codable {
    var id: Int?
    var name: String?
    var status: String?
    var gender: String?
    var image: String?
    var species: String?
    
    enum CodingKeys: String, CodingKey {
        case id, name, status, gender, image, species
    }
    
    init(from decoder: Decoder) throws {
        let container = try? decoder.container(keyedBy: CodingKeys.self)
        id = try container?.decode(Int?.self, forKey: .id)
        name = try container?.decode(String?.self, forKey: .name)
        status = try container?.decode(String?.self, forKey: .status)
        gender = try container?.decode(String?.self, forKey: .gender)
        image = try container?.decode(String?.self, forKey: .image)
        species = try container?.decode(String?.self, forKey: .species)
    }
}
